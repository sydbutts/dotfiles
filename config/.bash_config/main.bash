if [ $OSTYPE == "linux-gnu" ] && [ $HOSTTYPE == "x86_64" ]
then

    export PATH=$PATH:~/bin/:~/.emacs.d/bin
    source ~/.bash_config/completion.bash
    source ~/.bash_config/key-bindings.bash

    # use fd instead of find for fzf
    export FZF_DEFAULT_COMMAND='fd --type f'

    alias e='emacs'
    alias top='htop'
    alias b='browsh_1.6.4_linux_amd64'
    alias ls='~/bin/exa-linux-x86_64'
    alias cat='bat'
    alias docker='podman'
    alias fw+="sudo firewall-cmd --add-port"
    alias fw-="sudo firewall-cmd --remove-port"
    alias fw?="sudo firewall-cmd --list-all"
fi
